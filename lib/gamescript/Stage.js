function Stage(width, height) {
	this.width = width;
	this.height = height;

	this.actors = [];
	this.keyUpCallbacks = {};
	this.keyPersistentCallbacks = {};
}

Stage.prototype = {
	toString: function() {
		return "Stage [" + this.width + ", " + this.height + "]";
	},

	addActor: function(newActor) {
		this.actors.push(newActor);
	},

	tick: function() {
		for (var actor of this.actors) {
			actor.draw();
		}

		for (var actor of this.actors)
		    actor.update();

        for (var key of Reflect.ownKeys(this.keyPersistentCallbacks)) {
            if (keyIsDown(key.charCodeAt(0))) {
                for (var f of this.keyPersistentCallbacks[key])
                    f();
            }
        }

		for (var actor of this.actors) {
			actor.collisions(this.actors);
		}
	},

	registerKeyPersistentCallback: function(key, callback) {
        if (!this.keyPersistentCallbacks[key]) {
            this.keyPersistentCallbacks[key] = [];
        }
        this.keyPersistentCallbacks[key].push(callback);
    },

	registerKeyUpCallback: function(key, callback) {
		if (!this.keyUpCallbacks[key]) {
			this.keyUpCallbacks[key] = [];
		}
		this.keyUpCallbacks[key].push(callback);
	},

	keyUp: function(key) {
		if (this.keyUpCallbacks[key]) {
			for (var callback of this.keyUpCallbacks[key]) {
				callback();
			}
		}
	}
};
