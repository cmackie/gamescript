function Actor(id, x = 0, y = 0, dx = 0, dy = 0, width = 0, height = 0) {
    this.id = id;

    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;

    this.width = width;
    this.height = height;

	this.constantFun = [];
	this.constantFun.push(function (me) {
        me.x += me.dx;
        me.y += me.dy;
    });

	this.collisionMap = {};
}

Actor.prototype = {
	toString: function() {
		return "Actor [" + this.id + ", " + this.x + ", " + this.y + ", " + this.dx + ", " + this.dy +
            ", " + this.width + ", " + this.height + "]";
	},

    addConstantFun: function(f) {
	    this.constantFun.push(f);
    },

    addCollision: function(id, f) {
	    this.collisionMap[id] = f;
    },

    isColliding: function(actor) {
        var x = this.x;
        var y = this.y;
        var h = this.height;
        var w = this.width;
        var x2 = actor.x;
        var y2 = actor.y;
        var h2 = actor.height;
        var w2 = actor.width;
    
        return (x + w >= x2 &&  // r1 right edge past r2 left
                x <= x2 + w2 && // r1 left edge past r2 right
                y + h >= y2 &&  // r1 top edge past r2 bottom
                y <= y2 + h2);  // r1 bottom edge past r2 top

    },

	collisions: function(actors) {
	    for (var actor of actors) {
	        if (actor != this && this.isColliding(actor) && this.collisionMap[actor.id]) {
				this.collisionMap[actor.id](this);
            }
        }
    },

    update: function() {
	    for (var f of this.constantFun) {
	        f(this);
	    }
    },

	draw: function() { },

	move: function(direction, magnitude) {
        var rad = direction/360 * 2 * Math.PI;

        var dx = Math.cos(rad) * magnitude;
        var dy = Math.sin(rad) * magnitude;

        var me = this;

        return function() {
            me.x += dx;
            me.y += dy;
        }
    },

    force: function(direction, magnitude, maxSpeed=1000000000000000) {
        var rad = direction/360 * 2 * Math.PI;

        var ddx = Math.cos(rad) * magnitude;
        var ddy = Math.sin(rad) * magnitude;

        var me = this;

        return function() {
            me.dx += ddx;
            me.dy += ddy;
            if (me.dx > maxSpeed)
                me.dx = maxSpeed
            if (me.dx < -maxSpeed)
                me.dx = -maxSpeed;
            if (me.dy > maxSpeed)
                me.dy = maxSpeed
            if (me.dy < -maxSpeed)
                me.dy = -maxSpeed;
        }
    }
};
