parser grammar GameScriptParser;

options { language=JavaScript; tokenVocab='../lexer/GameScriptLexer'; }

document : actors? setup? EOF ;

actors : ACTORS actorDef+ ;

actorDef : actorName actorFields? actorBody ;

actorName : ID ;

actorFields returns [names]
    : ID	#singleActorField
    | ID actorFields	#multiActorFields
    ;

actorBody returns [properties, methods, collisions] :
    OPEN_BRACE actorFieldDefs? actorCollisions? CLOSE_BRACE ;

actorFieldDefs returns [properties, methods]
    : annotatedActorFieldDef	#singleFieldDef
    | annotatedActorFieldDef COMMA actorFieldDefs	#multiFieldDef
    ;

annotatedActorFieldDef returns [properties, methods]
    : actorFieldDef # noAnnotation
    | DOLLAR actorFieldDef # constantFunction
    ;

actorFieldDef returns [property, method, name]
    : ID COLON actorFieldValue	#propertyDef
    | ID actorFields? OPEN_BRACE actorMethodBody CLOSE_BRACE	#methodDef
    ;

actorFieldValue : expr ;

expr : orExpr ;

orExpr
    : andExpr # descendOrExpr
    | orExpr orBinop andExpr # trueOrExpr
    ;

orBinop : OR ;

andExpr
    : equalityExpr # descendAndExpr
    | andExpr andBinop equalityExpr # trueAndExpr
    ;

andBinop : AND ;

equalityExpr
    : relationalExpr # descendEqualityExpr
    | equalityExpr equalityBinop relationalExpr # trueEqualityExpr
    ;

equalityBinop
    : EQ
    | NEQ
    ;

relationalExpr
    : additiveExpr # descendRelationalExpr
    | relationalExpr relationalBinop additiveExpr # trueRelationalExpr
    ;

relationalBinop
    : LT
    | GT
    | LE
    | GE
    ;

additiveExpr
    : multiplicitiveExpr # descendAdditiveExpr
    | additiveExpr additiveBinop multiplicitiveExpr # trueAdditiveExpr
    ;

additiveBinop
    : PLUS
    | MINUS
    ;

multiplicitiveExpr
    : unopExpr # descendMultiplicitiveExpr
    | multiplicitiveExpr multiplicitiveBinop unopExpr # trueMultiplicitiveExpr
    ;

multiplicitiveBinop
    : TIMES
    | DIV
    ;

unopExpr
    : callExpr # descendUnopExpr
    | unop callExpr # trueUnopExpr
    ;

unop
    : MINUS
    | NOT
    ;

callExpr
    : primaryExpr # descendCallExpr
    | call # trueCallExpr
    ;

primaryExpr
    : litExpr
    | varExpr
    | parenExpr
    | accessExpr
    ;

litExpr :
      INTEGER
    | FLOAT
    | STRING
    | BOOLEAN
    ;

varExpr : ME? ID ;

parenExpr : OPEN_PAREN expr CLOSE_PAREN ;

accessExpr: ID DOT ID ;

call
    : callerExpr OPEN_PAREN CLOSE_PAREN # noArgumentCall
    | callerExpr callArgs? # argumentCall
    ;

callerExpr
    : varExpr
    | accessExpr
    ;

callArgs :
        primaryExpr # singleArg
      | primaryExpr callArgs # multiArgs
      ;

actorMethodBody : body ;

body :
      stmt # singleStmt
    | stmt body # multiStmt
    ;

stmt 
    : callStmt
    | assignStmt
    | controlStmt
    ;

callStmt : call SEMICOLON;

assignStmt : varExpr WS? GETS WS? expr SEMICOLON ;

controlStmt
    : ifStmt
    | whileStmt
    ;

ifStmt
    : IF OPEN_PAREN expr CLOSE_PAREN OPEN_BRACE body CLOSE_BRACE (elseStmt | elseIfStmt)? ;

elseStmt : ELSE OPEN_BRACE body CLOSE_BRACE ;

elseIfStmt : ELSE ifStmt ;

whileStmt : WHILE OPEN_PAREN expr CLOSE_PAREN OPEN_BRACE body CLOSE_BRACE ;

actorCollisions returns [collisions] :
    COLLISIONS WS? OPEN_SQUARE_BRACE WS? actorCollisionMappings WS? CLOSE_SQUARE_BRACE ;

actorCollisionMappings returns [collisions] :
      actorCollisionMapping # singleMapping
    | actorCollisionMapping WS? actorCollisionMappings # multiMappings
    ;

actorCollisionMapping returns [collisions] :
    ID WS? MAP WS? OPEN_BRACE WS? body WS? CLOSE_BRACE ;

setup : SETUP width height COLON setupBody ;

width
    : INTEGER
    | FLOAT
    ;

height
    : INTEGER
    | FLOAT
    ;

setupBody :
      setupStmt # singleSetupStmt
    | setupStmt WS? setupBody # multiSetupStmt
    ;

setupStmt 
    : stmt
    | actorStmt
    ;

actorStmt : ID WS? COLON WS? actorName WS? actorArgs persistentKeys? upKeys? SEMICOLON;

actorArgs
    : # noActorArgs
    | primaryExpr # singleActorArg
    | primaryExpr actorArgs # multiActorArgs
    ;

persistentKeys returns [keyMap] :
    OPEN_SQUARE_BRACE keyMappings CLOSE_SQUARE_BRACE ;

upKeys returns [keyMap] :
    OPEN_SQUARE_BRACE keyMappings CLOSE_SQUARE_BRACE ;

keyMappings returns [keyMap] :
      keyMapping # singleKeyMapping
    | keyMapping COMMA keyMappings # multiKeyMappings
    ;

keyMapping returns [keyMap] :
    key MAP varExpr ;

key : ID ;
