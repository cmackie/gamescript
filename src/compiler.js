var antlr4 = require('antlr4/index');
var GameScriptLexer = require('./lexer/GameScriptLexer').GameScriptLexer;
var GameScriptParser = require('./parser/GameScriptParser').GameScriptParser;
var GameScriptParserListener = require('./parser/GameScriptParserListener').GameScriptParserListener;

function GameScriptCompiler() {
	GameScriptParserListener.call(this);
	return this;
}

GameScriptCompiler.prototype = Object.create(GameScriptParserListener.prototype);
GameScriptCompiler.prototype.constructor = GameScriptCompiler;

GameScriptCompiler.escape = function (string) {
	return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (c) {
		switch (c) {
		case '"':
		case "'":
		case '\\':
			return '\\' + c;
		case '\n':
			return '\\n';
		case '\r':
			return '\\r';
		case '\u2028':
			return '\\u2028';
		case '\u2029':
			return '\\u2029';
		}
	});
};

GameScriptCompiler.prototype.compile = function (template) {
    this._bodySource =
`var WIDTH, HEIGHT;
var stage;
`;

	var chars = new antlr4.InputStream(template);
	var lexer = new GameScriptLexer(chars);
	var tokens = new antlr4.CommonTokenStream(lexer);
	var parser = new GameScriptParser(tokens);
	parser.buildParseTrees = true;
	var tree = parser.document();
	antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

	this._bodySource +=
`function draw() {
background(200);
stage.tick();
}
`;

	return this._bodySource;
};

GameScriptCompiler.prototype.compileWithHTML = function (template) {
	return this.htmlHead + this.compile(template) + this.htmlFoot;
};

GameScriptCompiler.prototype.append = function (expr) {
	this._bodySource += `${expr}` + `\n`;
};

GameScriptCompiler.prototype.exitActorName = function(ctx) {
	ctx.src = ctx.ID().getText();
};

GameScriptCompiler.prototype.enterActors = function(ctx) {
	this.append("// enter actors");
};

GameScriptCompiler.prototype.exitActors = function(ctx) {
	this.append("// exit actors");
};

GameScriptCompiler.prototype.enterActorDef = function(ctx) {
	this.append("// enter actor def");
};

GameScriptCompiler.prototype.exitActorDef = function(ctx) {
    var actorName = ctx.actorName().src;

	this.append("var " + actorName + " = " + "function (");

	var fieldNames = [];
	if (ctx.actorFields()) {
        this.append(ctx.actorFields().src);
        fieldNames = ctx.actorFields().names;
    }

	this.append(") {");
	this.append("var me = new Actor(\"" + actorName + "\");");

	for (var name of fieldNames)
	    this.append("me." + name + " = " + name + ";");

	if (ctx.actorBody().properties.length > 0) {
		for (var prop of ctx.actorBody().properties)
			this.append(prop);
	}

	this.append("// user defined functions will go here");
	if (ctx.actorBody().methods.length > 0) {
        for (var method of ctx.actorBody().methods)
            this.append(method);
	}
	this.append("// the collision map will go here");
	this.append("me.collisionMap = {");
	if (ctx.actorBody().collisions.length > 0) {
	    var collisions = ctx.actorBody().collisions[0];
	    for (var i = 1; i < ctx.actorBody().collisions.length; i++)
	        collisions += ",\n" + ctx.actorBody().collisions[i];

	    this.append(collisions);
    }
    this.append("};");
	this.append("return me;");
	this.append("};");
	this.append("// exit actor def");
};

GameScriptCompiler.prototype.exitSingleActorField = function(ctx) {
    ctx.src = ctx.ID().getText();
    ctx.names = [ctx.ID().getText()];
};

GameScriptCompiler.prototype.exitMultiActorFields = function(ctx) {
    ctx.src = ctx.ID().getText() + ",\n" + ctx.actorFields().src;
    ctx.names = [];
    ctx.names.push(ctx.ID().getText());
    for (var name of ctx.actorFields().names)
        ctx.names.push(name);
};

GameScriptCompiler.prototype.exitActorBody = function(ctx) {
    ctx.properties = [];
    ctx.methods = [];
    ctx.collisions = [];
	if (ctx.actorFieldDefs()) {
		ctx.properties = ctx.actorFieldDefs().properties;
		ctx.methods = ctx.actorFieldDefs().methods;
	}

	if (ctx.actorCollisions())
	    ctx.collisions = ctx.actorCollisions().collisions;
};

GameScriptCompiler.prototype.exitSingleFieldDef = function(ctx) {
	ctx.methods = ctx.annotatedActorFieldDef().methods;
	ctx.properties = ctx.annotatedActorFieldDef().properties;
};

GameScriptCompiler.prototype.exitMultiFieldDef = function(ctx) {
	var methods = ctx.annotatedActorFieldDef().methods;
	var properties = ctx.annotatedActorFieldDef().properties;

    for (var prop of ctx.actorFieldDefs().properties)
        properties.push(prop);

    for (var method of ctx.actorFieldDefs().methods)
        methods.push(method);

	ctx.methods = methods;
	ctx.properties = properties;
};

GameScriptCompiler.prototype.exitNoAnnotation = function(ctx) {
    ctx.properties = [];
    ctx.methods = [];
    if (ctx.actorFieldDef().property)
        ctx.properties = [ctx.actorFieldDef().property];
    if (ctx.actorFieldDef().method)
        ctx.methods = [ctx.actorFieldDef().method];
};

GameScriptCompiler.prototype.exitConstantFunction = function(ctx) {
    ctx.properties = [];
    ctx.methods = [];
    var constantFun = "\nme.addConstantFun(me." + ctx.actorFieldDef().name + ");";
    if (ctx.actorFieldDef().property)
        ctx.properties = [ctx.actorFieldDef().property + constantFun];
    if (ctx.actorFieldDef().method)
        ctx.methods = [ctx.actorFieldDef().method + constantFun];
};

GameScriptCompiler.prototype.exitPropertyDef = function(ctx) {
	ctx.property = "me." + ctx.ID().getText() + " = " + ctx.actorFieldValue().src + ";";
	ctx.name = ctx.ID().getText();
};

GameScriptCompiler.prototype.exitMethodDef = function(ctx) {
    var method = "me." + ctx.ID().getText() + " = function(\n";

    if (ctx.actorFields())
        method += ctx.actorFields().src + "\n";

	ctx.method = method + ") {\n" + ctx.actorMethodBody().src + "\n};";
	ctx.name = ctx.ID().getText();
};

GameScriptCompiler.prototype.exitActorFieldValue = function(ctx) {
    ctx.src = ctx.expr().src
};

GameScriptCompiler.prototype.exitExpr = function(ctx) {
    ctx.src = ctx.orExpr().src;
};

GameScriptCompiler.prototype.exitDescendOrExpr = function(ctx) {
    ctx.src = ctx.andExpr().src;
};

GameScriptCompiler.prototype.exitTrueOrExpr = function(ctx) {
    ctx.src = ctx.orExpr().src + ctx.orBinop().src + ctx.andExpr().src;
};

GameScriptCompiler.prototype.exitOrBinop = function(ctx) {
    ctx.src = ctx.OR().getText();
};

GameScriptCompiler.prototype.exitDescendAndExpr = function(ctx) {
    ctx.src = ctx.equalityExpr().src;
};

GameScriptCompiler.prototype.exitTrueAndExpr = function(ctx) {
    ctx.src = ctx.andExpr().src + ctx.andBinop().src + ctx.equalityExpr().src;
};

GameScriptCompiler.prototype.exitAndBinop = function(ctx) {
    ctx.src = ctx.AND().getText();
};

GameScriptCompiler.prototype.exitDescendEqualityExpr = function(ctx) {
    ctx.src = ctx.relationalExpr().src;
};

GameScriptCompiler.prototype.exitTrueEqualityExpr = function(ctx) {
    ctx.src = ctx.equalityExpr().src + ctx.equalityBinop().src + ctx.relationalExpr().src;
};

GameScriptCompiler.prototype.exitEqualityBinop = function(ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitDescendRelationalExpr = function(ctx) {
    ctx.src = ctx.additiveExpr().src;
};

GameScriptCompiler.prototype.exitTrueRelationalExpr = function(ctx) {
    ctx.src = ctx.relationalExpr().src + ctx.relationalBinop().src + ctx.additiveExpr().src;
};

GameScriptCompiler.prototype.exitRelationalBinop = function(ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitDescendAdditiveExpr = function(ctx) {
    ctx.src = ctx.multiplicitiveExpr().src;
};

GameScriptCompiler.prototype.exitTrueAdditiveExpr = function(ctx) {
    ctx.src = ctx.additiveExpr().src + ctx.additiveBinop().src + ctx.multiplicitiveExpr().src;
};

GameScriptCompiler.prototype.exitAdditiveBinop = function(ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitDescendMultiplicitiveExpr = function(ctx) {
    ctx.src = ctx.unopExpr().src;
};

GameScriptCompiler.prototype.exitTrueMultiplicitiveExpr = function(ctx) {
    ctx.src = ctx.multiplicitiveExpr().src + ctx.multiplicitiveBinop().src + ctx.unopExpr().src;
};

GameScriptCompiler.prototype.exitMultiplicitiveBinop = function(ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitDescendUnopExpr = function(ctx) {
    ctx.src = ctx.callExpr().src;
};

GameScriptCompiler.prototype.exitTrueUnopExpr = function(ctx) {
    ctx.src = ctx.unop().src + ctx.callExpr().src;
};

GameScriptCompiler.prototype.exitUnop = function(ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitDescendCallExpr = function(ctx) {
    ctx.src = ctx.primaryExpr().src;
};

GameScriptCompiler.prototype.exitTrueCallExpr = function(ctx) {
    ctx.src = ctx.call().src;
};

GameScriptCompiler.prototype.exitPrimaryExpr = function(ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitLitExpr = function(ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitVarExpr = function(ctx) {
    ctx.src = "";
    if (ctx.ME())
        ctx.src += "me.";
    ctx.src += ctx.ID().getText();
};

GameScriptCompiler.prototype.exitParenExpr = function(ctx) {
    ctx.src = "(" + ctx.expr().src + ")";
};

GameScriptCompiler.prototype.exitAccessExpr = function(ctx) {
	var ids = ctx.ID();
	ctx.src = ids[0] + '.' + ids[1];
};

GameScriptCompiler.prototype.exitNoArgumentCall = function (ctx) {
    ctx.src = ctx.callerExpr().src + "()";
};

GameScriptCompiler.prototype.exitArgumentCall = function (ctx) {
    var args = ctx.callArgs() ? ctx.callArgs().src : "";
	ctx.src = ctx.callerExpr().src + "(" + args + ")";
};

GameScriptCompiler.prototype.exitCallerExpr = function(ctx) {
    ctx.src = ctx.children[0].src;
};

GameScriptCompiler.prototype.exitAssignStmt = function(ctx) {
	ctx.src = ctx.getText();
};

GameScriptCompiler.prototype.exitSingleArg = function (ctx) {
	ctx.src = ctx.primaryExpr().src;
};

GameScriptCompiler.prototype.exitMultiArgs = function (ctx) {
	ctx.src = ctx.primaryExpr().src + ", " + ctx.callArgs().src;
};

GameScriptCompiler.prototype.exitActorMethodBody = function(ctx) {
    ctx.src = ctx.body().src;
};

GameScriptCompiler.prototype.exitSingleStmt = function(ctx) {
    ctx.src = ctx.stmt().src;
};

GameScriptCompiler.prototype.exitMultiStmt = function(ctx) {
    ctx.src = ctx.stmt().src + "\n" + ctx.body().src;
};

GameScriptCompiler.prototype.exitStmt = function(ctx) {
    ctx.src = ctx.children[0].src;
};

GameScriptCompiler.prototype.exitCallStmt = function(ctx) {
    ctx.src = ctx.call().src + ";";
};

GameScriptCompiler.prototype.exitAssignStmt = function(ctx) {
    ctx.src = ctx.varExpr().src + " = " + ctx.expr().src + ";";
};

GameScriptCompiler.prototype.exitControlStmt = function(ctx) {
    ctx.src = ctx.children[0].src;
};

GameScriptCompiler.prototype.exitIfStmt = function(ctx) {
    ctx.src = "if(" + ctx.expr().src + "){\n" + ctx.body().src + "\n}";

    if (ctx.elseStmt())
        ctx.src += ctx.elseStmt().src;
    else if (ctx.elseIfStmt())
        ctx.src += ctx.elseIfStmt().src;
};

GameScriptCompiler.prototype.exitElseStmt = function(ctx) {
    ctx.src = "else{\n" + ctx.body().src + "\n}";
};

GameScriptCompiler.prototype.exitElseIfStmt = function(ctx) {
    ctx.src = "else " + ctx.ifStmt().src;
};

GameScriptCompiler.prototype.exitWhileStmt = function(ctx) {
    ctx.src = "while(" + ctx.expr().src + "){\n" + ctx.body().src + "\n}";
};

GameScriptCompiler.prototype.exitActorCollisions = function(ctx) {
    ctx.collisions = ctx.actorCollisionMappings().collisions;
};

GameScriptCompiler.prototype.exitSingleMapping = function(ctx) {
    ctx.collisions = ctx.actorCollisionMapping().collisions;
};

GameScriptCompiler.prototype.exitMultiMappings = function(ctx) {
    ctx.collisions = ctx.actorCollisionMapping().collisions;

    for (var collision of ctx.actorCollisionMappings().collisions)
        ctx.collisions.push(collision);
};

GameScriptCompiler.prototype.exitActorCollisionMapping = function(ctx) {
    ctx.collisions = [ctx.ID().getText() + ": function() {\n" + ctx.body().src + "\n}"];
};

GameScriptCompiler.prototype.exitSetup = function (ctx) {
	this.append("function setup() {\n"
        + "WIDTH = " + ctx.width().src + ";\n"
        + "HEIGHT = " + ctx.height().src + ";\n"
        + "createCanvas(WIDTH, HEIGHT);\n"
        + "stage = new Stage(WIDTH, HEIGHT);\n"
        + ctx.setupBody().src + "\n}");
};

GameScriptCompiler.prototype.exitWidth = function (ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitHeight = function (ctx) {
    ctx.src = ctx.children[0].getText();
};

GameScriptCompiler.prototype.exitSingleSetupStmt = function(ctx) {
    ctx.src = ctx.setupStmt().src;
};

GameScriptCompiler.prototype.exitMultiSetupStmt = function(ctx) {
    ctx.src = ctx.setupStmt().src + "\n" + ctx.setupBody().src;
};

GameScriptCompiler.prototype.exitSetupStmt = function(ctx) {
    ctx.src = ctx.children[0].src;
};

GameScriptCompiler.prototype.exitActorStmt = function(ctx) {
    var name = ctx.ID().getText();

    ctx.src = "var " + name + " = " + ctx.actorName().src + "(" + ctx.actorArgs().src + ");";
    if (ctx.persistentKeys()) {
        for (var persistentKey of Reflect.ownKeys(ctx.persistentKeys().keyMap))
            ctx.src += "\nstage.registerKeyPersistentCallback(\"" + persistentKey + "\", " + name + "." + ctx.persistentKeys().keyMap[persistentKey] + ");";
    }

    if (ctx.upKeys()) {
        for (var upKey of Reflect.ownKeys(ctx.upKeys().keyMap))
            ctx.src += "\nstage.registerKeyUpCallback(\"" + upKey + "\", " + name + "." + ctx.upKeys().keyMap[upKey] + ");";
    }
    ctx.src += "\nstage.addActor(" + name + ");";
};

GameScriptCompiler.prototype.exitNoActorArgs = function(ctx) {
    ctx.src = "";
};

GameScriptCompiler.prototype.exitSingleActorArg = function(ctx) {
    ctx.src = ctx.primaryExpr().src;
};

GameScriptCompiler.prototype.exitMultiActorArgs = function(ctx) {
    ctx.src = ctx.primaryExpr().src + ", " + ctx.actorArgs().src;
};

GameScriptCompiler.prototype.exitPersistentKeys = function(ctx) {
    ctx.keyMap = ctx.keyMappings().keyMap;
};

GameScriptCompiler.prototype.exitUpKeys = function(ctx) {
    ctx.keyMap = ctx.keyMappings().keyMap;
};

GameScriptCompiler.prototype.exitSingleKeyMapping = function(ctx) {
    ctx.keyMap = ctx.keyMapping().keyMap;
};

GameScriptCompiler.prototype.exitMultiKeyMappings = function(ctx) {
    ctx.keyMap = ctx.keyMapping().keyMap;

    for (var key of Reflect.ownKeys(ctx.keyMappings().keyMap))
        Reflect.set(ctx.keyMap, key, ctx.keyMappings().keyMap[key]);
};

GameScriptCompiler.prototype.exitKeyMapping = function(ctx) {
    ctx.keyMap = {};
    Reflect.set(ctx.keyMap, ctx.key().src, ctx.varExpr().src);
};

GameScriptCompiler.prototype.exitKey = function(ctx) {
    ctx.src = ctx.ID().getText();
};

GameScriptCompiler.prototype.htmlHead =
`<!DOCTYPE html>
<html>
<head>
<title>GameScript!</title>
<script src="./lib/p5/p5.js"></script>
<script src="./lib/p5/p5.play.js"></script>
<script src="./lib/gamescript/Stage.js"></script>
<script src="./lib/gamescript/Actor.js"></script>
<script type=text/javascript>
`;

GameScriptCompiler.prototype.htmlFoot = 
`</script>
</head>
<body>
<h1>Your Game</h1>
</body>
</html>
`;

exports.GameScriptCompiler = GameScriptCompiler;
