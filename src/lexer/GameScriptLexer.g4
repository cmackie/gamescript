lexer grammar GameScriptLexer;

options { language=JavaScript; }

// Default mode

// keywords
ACTORS  : 'actors:' ;
SETUP   : 'setup' ;
LOOP    : 'loop:' ;

ME	: 'me.' ;

IF : 'if' ;
ELSE : 'else';
WHILE : 'while' ;

COLLISIONS  : 'collisions' ;

// Integer and float literals
INTEGER : '-'? INT ;
FLOAT
    :   '-'? INT '.' INT EXP?
    |   '-'? INT EXP
    ;

fragment INT : '0' | [1-9] [0-9]* ;
fragment EXP : [Ee] [+\-]? INT ;

// String literal
STRING :  '\'' (ESC | ~['\\])* '\'' ;

fragment HEX : [0-9a-fA-F] ;
fragment UNICODE : 'u' HEX HEX HEX HEX ;
fragment ESC :   '\\' (['\\/bfnrt] | UNICODE) ;

// Boolean literals
BOOLEAN
    : 'true'
    | 'false'
    ;

// Miscellaneous symbols
OPEN_PAREN : '(' ;
CLOSE_PAREN : ')' ;

OPEN_BRACE    : '{' ;
CLOSE_BRACE  : '}' ;

OPEN_SQUARE_BRACE  : '[' ;
CLOSE_SQUARE_BRACE : ']' ;

DOT: '.' ;
COLON : ':' ;
SEMICOLON : ';' ;
COMMA: ',' ;
DOLLAR: '$' ;

GETS : '=' ;
PLUS : '+' ;
TIMES : '*' ;
DIV : '/' ;
LT : '<' ;
GT : '>' ;
LE : '<=' ;
GE : '>=' ;
EQ : '==' ;
NEQ : '!=' ;
AND : '&&' ;
OR : '||' ;
MINUS : '-' ;
NOT : '!' ;

MAP : '->' ;

BLOCK_COMMENT : '/*' .*? '*/' -> channel(HIDDEN) ;

ID : [a-zA-Z_] [a-zA-Z0-9_]* ;

WS : [ \t\n\r]+ -> channel(HIDDEN);
