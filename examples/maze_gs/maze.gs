actors:
    player x y {
        width: 5,
        height: 5,
        up: me.force 270 0.5 5,
        down: me.force 90 0.5 5,
        left: me.force 180 0.5 5,
        right: me.force 0 0.5 5,
        draw {
            fill 120 120 120;
            ellipse me.x me.y me.width;
        }
        collisions [
            side -> {
                me.dx = -me.dx;
            }
            topp -> {
                me.dy = -me.dy;
            }
            wall -> {

                me.dx = -me.dx;
                me.dy = -me.dy;
            }
            goal -> {
            	me.dx = 0;
            	me.dy = 0;
            	fill 255 255 255;
            	rect -1 -1 301 301;
            	fill 0;
            	stroke 0;
            	text 'YOU WIN' 150 150;
            	noLoop;
            }
        ]
    }
    topp x y {
        height: 10,
        width: WIDTH,
        draw {
            fill 0;
            rect me.x me.y me.width me.height;
        }
    }
    side x y {
        height: HEIGHT,
        width: 10,
        draw {
            fill 0;
            rect me.x me.y me.width me.height;
        }
    }
    wall x y width height {
        draw {
            fill 100;
            rect me.x me.y me.width me.height;
        }
    }

    goal x y width height {
    	draw {
            fill 124 252 0;
            rect me.x me.y me.width me.height;
    	}
    }

setup 300 300:
    p: player 20 20 [
        W -> up,
        A -> left,
        S -> down,
        D -> right
    ];

    t: topp 0 0;
    b: topp 0 (HEIGHT - 10);
    l: side 0 0;
    r: side (WIDTH - 10) 0;
    w1: wall 40 10 10 250;
    w2: wall 200 150 10 30;
    w3: wall 250 10 10 250;
    w4: wall 150 40 10 250;
    g: goal (WIDTH - (20 + 10)) 10 20 20;
