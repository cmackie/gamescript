actors:
    bar x y {
        width: 20,
        height: 80,
        up: me.move 270 5,
        down: me.move 90 5,

        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }
    }

    bumper y {
        x: 0,
        width: 300,
        height: 20,

        draw {
            fill 0 0 0;
            rect me.x me.y me.width me.height;
        }
    }

    score x isLeft {
        y: 0,
        width: 20,
        height: 300,
        n: 0,

        draw {
            fill 0 0 0;
            rect me.x me.y me.width me.height;
            textSize 50;
            textStyle BOLD;

            if (isLeft) {
                text me.n (me.x + 100) 100;
            } else {
                text me.n (me.x - 100) 100;
            }
        }

        collisions [
            ball -> { me.n = me.n + 1; }
        ]
    }

	ball x y dx dy {
	    width: 20,
	    height: 20,

		draw {
		    fill 0 0 255;
			ellipse (me.x + 10) (me.y + 10) me.width me.height;
		}

		collisions [
			bar -> { me.dx = -me.dx; }
			bumper -> { me.dy = -me.dy; }
			score -> {
			    me.x = 140;
			    me.y = 140;
			}
		]
	}

setup 300 300:
	p1: bar (WIDTH / 8 - 10) (HEIGHT / 2 - 40) [
	    W -> up,
	    S -> down
	];

	p2: bar (7 * WIDTH / 8 - 10) (HEIGHT / 2 - 40) [
		O -> up,
		L -> down
	];

	top: bumper 0;
	bottom: bumper (HEIGHT - 20);

	left: score 0 true;
	right: score (WIDTH - 20) false;

	baller: ball (WIDTH / 2 - 10) (HEIGHT / 2 - 10) 3 2;