var WIDTH = 300;
var HEIGHT = 300;

var stage;

// called by p5
function setup() {
	createCanvas(WIDTH, HEIGHT);
	stage = new Stage(WIDTH, HEIGHT);
	var a1 = new Actor("a1", WIDTH / 2, HEIGHT / 2);
	a1.draw = function() {
		fill(255, 0, 0);
		ellipse(this.x, this.y, 10, 10);
	};

	
	var a2 = new Actor("a2", 0.25 * WIDTH, 0.25 * HEIGHT);
	a2.draw = function() {
		fill(0,0, 255);
		ellipse(this.x, this.y, 20, 20);
	};
	stage.addActor(a1);
	stage.addActor(a2);
}

// called by p5
function draw() {
	background (200);
	stage.tick();
}
