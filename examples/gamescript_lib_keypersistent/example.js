var WIDTH = 300;
var HEIGHT = 300;

var stage;

// called by p5
function setup() {
	createCanvas(WIDTH, HEIGHT);
	stage = new Stage(WIDTH, HEIGHT);
	
	var a1 = new Actor("a", 0.25 * WIDTH, HEIGHT / 2, 0, 0);
	a1.draw = function() {
		fill(255, 0, 0);
		rect(this.x - 10, this.y - 10, 20, 20);
	};

	stage.addActor(a1);
	stage.registerKeyPersistentCallback("W", a1.move(270, 1));
	stage.registerKeyPersistentCallback("A", a1.move(180, 1));
	stage.registerKeyPersistentCallback("S", a1.move(90, 1));
	stage.registerKeyPersistentCallback("D", a1.move(0, 1));
}

// called by p5
function draw() {
	background (200);
	stage.tick();
}

function keyPressed() {
}



