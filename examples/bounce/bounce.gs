actors:
    a x y {
        width: 20,
        height: 20,
        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }
    }

    b x y dx {
        width: 10,
        height: 10,
        draw {
            fill 0 0 255;
            ellipse (me.x + 5) (me.y + 5) me.width me.height;
        }

        collisions [
            a -> { me.dx = -me.dx; }
        ]
    }

setup 300 300:
    left: a (0.25 * WIDTH - 10) (0.5 * HEIGHT - 10);
    right: a (0.75 * WIDTH - 10) (0.5 * HEIGHT - 10);
    ball: b (0.5 * WIDTH - 5) (0.5 * HEIGHT - 5) 5;