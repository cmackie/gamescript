actors:
	/*
	 * the player control this actor.
	 * it needs to accept the score actor to update the score display
	 */
	Player score {
		x: (WIDTH / 2),
		y: HEIGHT,
		width: 20,
		height: 20,
		left: me.force 180 1 3,
		right: me.force 0 1 3,

		/* Draw a player as a black square with a red circle on top */
		draw {
			fill 0;
			rect me.x (me.y - me.height) me.width me.height;
			fill 255 0 0;
			ellipse (me.x + (me.width / 2)) (me.y - me.height) (me.width / 2) (me.height / 2);
		}

		/* when a player collides with a wall, bounce it back! */
		collisions [
			Wall -> {
				me.dx = -me.dx;
			}
		]
	}

	/*
	 * Enemies to shoot at!
	 */
	Enemy x y score {
		width: 20,
		height: 20,
		dx: 0,
		dy: 1,

		/* Enemies are a bright pinkish square */
		draw {
			fill 200 100 200;
			rect me.x me.y me.width me.height;
		},

		/* On each frame, make sure to check that we're still on the screen! */
		$bounds {
			if (me.y > HEIGHT + 10) {
				me.y = -20;
				me.x = (Math.random()) * WIDTH;
				score.incMisses; /* record the miss with score! */
			}
		}

		/* Define how an enemy reacts to colliding with a bullet */
		collisions [
			Bullet -> {
				me.width = me.width * 0.6;
				me.height = me.height * 0.6;
				if ((me.width < 4)) {
					score.incKills; /* record a kill with score! */
					me.width = 20;
					me.height = 20;
					me.y = -20;
					me.x = ((Math.random()) * WIDTH);
					me.dy = me.dy + 0.25; /* make enemies faster */
				}
			}
		]
	}

	/* Define the bullet players will shoot! */
	Bullet x y player score {
		width: 2,
		height: 5,
		dx: 0,
		dy: -2,

		/* Players only have one bullet. They need to manage its flight 
		   carefully before shooting again! */
		shoot {
			/* make sure the bullet starts where the player is! */
			me.x = player.x + (player.width / 2) - (me.width / 2);
			me.y = player.y;
		},
		draw {
			fill 50;
			rect me.x me.y me.width me.height;
		}
	}

	/* Walls are meant to be collided with */
	Wall x {
		height: HEIGHT,
		width: 10,
		y: 0
	}

	/* 
	 * The score keeps track of kills and misses. It 
	 * prints them to the screen each frame 
	 */
	Score {
		kills: 0,
		misses: 0,
		incKills {
			me.kills = me.kills + 1;
		},
		incMisses {
			me.misses = me.misses + 1;
		},
		draw {
			stroke 0 255 0;
			text ('kills:\t' + me.kills) 10 10;
			stroke 255 0 0;
			text ('misses:\t' + me.misses) 10 20;
		}
	}

/* Create a 300x300 play area! */
setup 300 300:
	/* Make a score to give to all bullets and enemies */
	s: Score;

	/* make a player and map A and D to left and right movement */
	p1: Player [
		A -> left,
		D -> right
	];

	/* 
	 * Make a bunch of enemies at random x locations and various heights 
	 * Make sure enemies get the score, they'll need to record misses and deaths
	 */
	e1: Enemy ((Math.random()) * WIDTH) -10 s;
	e2: Enemy ((Math.random()) * WIDTH) -40 s;
	e3: Enemy ((Math.random()) * WIDTH) -30 s;
	e4: Enemy ((Math.random()) * WIDTH) -100 s;
	e5: Enemy ((Math.random()) * WIDTH) -130 s;
	e6: Enemy ((Math.random()) * WIDTH) -120 s;
	e7: Enemy ((Math.random()) * WIDTH) -150 s;
	
	/* make the player's bullet. Press V to shoot! */
	b8: Bullet (p1.x + (p1.width / 2)) (HEIGHT) p1 s [
		V -> shoot
	];

	/* make the bounding walls to keep the player in! */
	w1: Wall -10;
	w2: Wall WIDTH;