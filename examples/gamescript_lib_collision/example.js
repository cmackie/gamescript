var WIDTH = 300;
var HEIGHT = 300;

var stage;

// called by p5
function setup() {
	createCanvas(WIDTH, HEIGHT);
	stage = new Stage(WIDTH, HEIGHT);
	var a1 = new Actor("a", 0.25 * WIDTH - 10, HEIGHT / 2 - 10, 0, 0, 20, 20);
	a1.draw = function() {
		fill(255, 0, 0);
		rect(this.x, this.y, this.width, this.height);
	};
	
	var a2 = new Actor("a", 0.75 * WIDTH - 10, HEIGHT / 2 - 10, 0, 0, 20, 20);
	a2.draw = function() {
		fill(255,0, 0);
		rect(this.x, this.y, this.width, this.height);
	};

	var mid = new Actor("mid", WIDTH / 2 - 5, HEIGHT / 2 - 5, 5, 0, 10, 10);
	mid.draw = function() {
	    fill(0, 0, 255);
	    ellipse(this.x + 5, this.y + 5, this.width, this.height);
    };
	mid.addCollision("a", function(me) {
	    me.dx = -me.dx;
    });

	stage.addActor(a1);
	stage.addActor(a2);
	stage.addActor(mid)
}

// called by p5
function draw() {
	background (200);
	stage.tick();
}
