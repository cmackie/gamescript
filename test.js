mocha.setup("bdd");
var expect = chai.expect;

var Compiler = require("./src/compiler").GameScriptCompiler;

describe("Game engine", function() {
	describe("Actor", function() {
		it("Verifies basic toString", function() {
			var actor = new Actor("actor", 1, 10);
			var expected = "Actor [actor, 1, 10, 0, 0, 0, 0]";
			expect(actor.toString()).to.equal(expected);
		});
	});
	
	describe("Player", function() {
	});

	describe("Stage", function() {
		it("Verifies basic toString", function() {
			var stage = new Stage(1, 10);
			var expected = "Stage [1, 10]";
			expect(stage.toString()).to.equal(expected);
		});
		
		it ("Stage tick invokes update and draw on actors, and self collisions aren't detected", function() {
			var stage = new Stage(100, 100);
			var actor1 = new Actor("a1", 50, 50);
			var actor2 = new Actor("a2", 50, 40, 0, 5);

			actor1.collisionsDetected = 0;
			actor2.collisionsDetected = 0;

			actor1.addCollision("a2", function (me) {
			    me.collisionsDetected++;
            });

            actor1.addCollision("a1", function (me) {
                me.collisionsDetected++;
            });

            actor2.addCollision("a1", function (me) {
                me.collisionsDetected++;
            });

            actor2.addCollision("a2", function (me) {
                me.collisionsDetected++;
            });

            actor1.timesDrawn = 0;
            actor2.timesDrawn = 0;

            actor1.draw = function () {
                this.timesDrawn++;
            };

            actor2.draw = function () {
                this.timesDrawn++;
            };

            stage.addActor(actor1);
            stage.addActor(actor2);

            expect(actor1.toString()).to.equal("Actor [a1, 50, 50, 0, 0, 0, 0]");
            expect(actor2.toString()).to.equal("Actor [a2, 50, 40, 0, 5, 0, 0]");

            stage.tick();

            expect(actor1.toString()).to.equal("Actor [a1, 50, 50, 0, 0, 0, 0]");
            expect(actor2.toString()).to.equal("Actor [a2, 50, 45, 0, 5, 0, 0]");
            expect(actor1.collisionsDetected).to.equal(0);
            expect(actor2.collisionsDetected).to.equal(0);
            expect(actor1.timesDrawn).to.equal(1);
            expect(actor2.timesDrawn).to.equal(1);

            stage.tick();

            expect(actor1.toString()).to.equal("Actor [a1, 50, 50, 0, 0, 0, 0]");
            expect(actor2.toString()).to.equal("Actor [a2, 50, 50, 0, 5, 0, 0]");
            expect(actor1.collisionsDetected).to.equal(1);
            expect(actor2.collisionsDetected).to.equal(1);
            expect(actor1.timesDrawn).to.equal(2);
            expect(actor2.timesDrawn).to.equal(2);
		});

        it("Key event callbacks are called by stage", function() {
            var stage = new Stage(100, 100);
            var a = 0;
            var b = 0;
            var b2 = 0;
            var c = 0;
            var d = 0;

            stage.registerKeyUpCallback("a", function() {
                a++;
            });
            stage.registerKeyUpCallback("b", function() {
                b++;
            });
            stage.registerKeyUpCallback("cd", function() {
                c++;
                d++;
            });
            stage.registerKeyUpCallback("b", function() {
                b2++;
            });

            stage.keyUp("a");
            stage.keyUp("b");
            stage.keyUp("cd");

            chai.expect(a).to.equal(1);
            chai.expect(b).to.equal(1);
            chai.expect(b2).to.equal(1);
            chai.expect(c).to.equal(1);
            chai.expect(d).to.equal(1);
        });
	});
});

describe("Compiler", function() {
    describe("Buildup to pong.gs", function() {
        it("compiles actor with no fields, parameters, or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with no fields, parameters, or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        
    }
    
    bumper {
    
    }
    
    score {
    
    }
    
    ball {
    
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var bumper = function (
) {
var me = new Actor("bumper");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
) {
var me = new Actor("score");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
) {
var me = new Actor("ball");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles actor with single field but no parameters, or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bumper y {
        
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bumper = function (
y
) {
var me = new Actor("bumper");
me.y = y;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with single field but no parameters, or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar x {
        
    }
    
    bumper y {
    
    }
    
    score x {
    
    }
    
    ball x {
    
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
x
) {
var me = new Actor("bar");
me.x = x;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var bumper = function (
y
) {
var me = new Actor("bumper");
me.y = y;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
x
) {
var me = new Actor("score");
me.x = x;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
x
) {
var me = new Actor("ball");
me.x = x;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles actor with multiple fields but no parameters, or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar x y {
        
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
x,
y
) {
var me = new Actor("bar");
me.x = x;
me.y = y;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with multiple fields but no parameters, or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar x y {
        
    }
    
    bumper y {
    
    }
    
    score x {
    
    }
    
    ball x y dx dy {
    
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
x,
y
) {
var me = new Actor("bar");
me.x = x;
me.y = y;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var bumper = function (
y
) {
var me = new Actor("bumper");
me.y = y;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
x
) {
var me = new Actor("score");
me.x = x;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
x,
y,
dx,
dy
) {
var me = new Actor("ball");
me.x = x;
me.y = y;
me.dx = dx;
me.dy = dy;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with a single property and no fields or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        width: 2
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.width = 2;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with a single non-literal property and no fields or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        up: me.move 270 5
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.up = me.move(270, 5);
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with a single property and no fields or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        width: 2
    }
    
    bumper {
        x: 0
    }
    
    score {
        y: 0
    }
    
    ball {
        width: 5
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.width = 2;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var bumper = function (
) {
var me = new Actor("bumper");
me.x = 0;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
) {
var me = new Actor("score");
me.y = 0;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
) {
var me = new Actor("ball");
me.width = 5;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with various properties and no fields or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        width: 2,
        height: 20,
        up: me.move 270 5,
        down: me.move 90 5
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.width = 2;
me.height = 20;
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with various properties and no fields or methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        width: 2,
        height: 20,
        up: me.move 270 5,
        down: me.move 90 5
    }
    
    bumper {
        x: 0,
        width: 100,
        height: 1
    }
    
    score {
        y: 0,
        width: 1,
        height: 100,
        red: 0
    }
    
    ball {
        width: 5,
        height: 5
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.width = 2;
me.height = 20;
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var bumper = function (
) {
var me = new Actor("bumper");
me.x = 0;
me.width = 100;
me.height = 1;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
) {
var me = new Actor("score");
me.y = 0;
me.width = 1;
me.height = 100;
me.red = 0;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
) {
var me = new Actor("ball");
me.width = 5;
me.height = 5;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with a single method which takes no parameters and no fields or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }  
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template)
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with a single method which takes a parameter and no fields or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        draw red {
            fill red 0 0;
            rect me.x me.y me.width me.height;
        }  
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.draw = function(
red
) {
fill(red, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with a single method which takes some parameters and no fields or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        draw red blue {
            fill red blue 0;
            rect me.x me.y me.width me.height;
        }  
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.draw = function(
red,
blue
) {
fill(red, blue, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with a single method with no parameters and no fields or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }  
    }
    
    score {
        draw {
            fill me.red 0 0;
            rect me.x me.y me.width me.height;
        }
    }
    
    ball {
        draw {
            fill 0 0 255;
            ellipse me.x me.y me.width me.height;
        }
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
) {
var me = new Actor("score");
// user defined functions will go here
me.draw = function(
) {
fill(me.red, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
) {
var me = new Actor("ball");
// user defined functions will go here
me.draw = function(
) {
fill(0, 0, 255);
ellipse(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with multiple methods and no fields or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        },  
        
        test {
            me.dx = -me.dx;
        }
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
me.test = function(
) {
me.dx = -me.dx;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with multiple methods with no parameters and no fields or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        },  
        
        test d {
            me.x = d;
        }
    }
    
    score {
        draw {
            fill me.red 0 0;
            rect me.x me.y me.width me.height;
        },
        
        test d dd {
            me.x = d;
            me.dx = dd;
        }
    }
    
    ball {
        draw {
            fill 0 0 255;
            ellipse me.x me.y me.width me.height;
        },
        
        test {
            me.x = 0;
        }
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
me.test = function(
d
) {
me.x = d;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
) {
var me = new Actor("score");
// user defined functions will go here
me.draw = function(
) {
fill(me.red, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
me.test = function(
d,
dd
) {
me.x = d;
me.dx = dd;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
) {
var me = new Actor("ball");
// user defined functions will go here
me.draw = function(
) {
fill(0, 0, 255);
ellipse(me.x, me.y, me.width, me.height);
};
me.test = function(
) {
me.x = 0;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with various properties, fields, and methods", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar x y {
        width: 2,
        height: 20,
        up: me.move 270 5,
        down: me.move 90 5,
        
        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
x,
y
) {
var me = new Actor("bar");
me.x = x;
me.y = y;
me.width = 2;
me.height = 20;
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with various properties, fields, and methods", function () {
            var c = new Compiler();
            var template =
            `actors:
    bar x y {
        width: 2,
        height: 20,
        up: me.move 270 5,
        down: me.move 90 5,

        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }
    }

    bumper y {
        x: 0,
        width: 100,
        height: 1
    }

    score x {
        y: 0,
        width: 1,
        height: 100,
        red: 0,

        draw {
            fill me.red 0 0;
            rect me.x me.y me.width me.height;
        }
    }

    ball x y dx dy {
        width: 5,
        height: 5,

        draw {
            fill 0 0 255;
            ellipse me.x me.y me.width me.height;
        }
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
x,
y
) {
var me = new Actor("bar");
me.x = x;
me.y = y;
me.width = 2;
me.height = 20;
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var bumper = function (
y
) {
var me = new Actor("bumper");
me.y = y;
me.x = 0;
me.width = 100;
me.height = 1;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
x
) {
var me = new Actor("score");
me.x = x;
me.y = 0;
me.width = 1;
me.height = 100;
me.red = 0;
// user defined functions will go here
me.draw = function(
) {
fill(me.red, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
x,
y,
dx,
dy
) {
var me = new Actor("ball");
me.x = x;
me.y = y;
me.dx = dx;
me.dy = dy;
me.width = 5;
me.height = 5;
// user defined functions will go here
me.draw = function(
) {
fill(0, 0, 255);
ellipse(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with a collision mapping and no fields, methods, or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    score {
        collisions [
            ball -> { me.red = 255; }
        ]
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var score = function (
) {
var me = new Actor("score");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
ball: function() {
me.red = 255;
}
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with a single collision mapping and no fields, methods, or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    score {
        collisions [
            ball -> { me.red = 255; }
        ]
    }
    
    ball {
        collisions [
            bar -> { me.dx = -me.dx; }
        ]
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var score = function (
) {
var me = new Actor("score");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
ball: function() {
me.red = 255;
}
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
) {
var me = new Actor("ball");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
bar: function() {
me.dx = -me.dx;
}
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with multiple collision mappings and no fields, methods, or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    ball {
        collisions [
            bar -> { me.dx = -me.dx; }
            bumper -> { me.dy = -me.dy; }
        ]
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var ball = function (
) {
var me = new Actor("ball");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
bar: function() {
me.dx = -me.dx;
},
bumper: function() {
me.dy = -me.dy;
}
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with collision mappings and no fields, methods, or properties", function () {
            var c = new Compiler();
            var template =
            `actors: 
    score {
        collisions [
            ball -> { me.red = 255; }
        ]
    }
    
    ball {
        collisions [
            bar -> { me.dx = -me.dx; }
            bumper -> { me.dy = -me.dy; }
        ]
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var score = function (
) {
var me = new Actor("score");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
ball: function() {
me.red = 255;
}
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
) {
var me = new Actor("ball");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
bar: function() {
me.dx = -me.dx;
},
bumper: function() {
me.dy = -me.dy;
}
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with various fields, properties, methods, and collision mappings", function () {
            var c = new Compiler();
            var template =
            `actors: 
    ball x y dx dy {
        width: 5,
        height: 5,

        draw {
            fill 0 0 255;
            ellipse me.x me.y me.width me.height;
        }
        
        collisions [
            bar -> { me.dx = -me.dx; }
            bumper -> { me.dy = -me.dy; }
        ]
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var ball = function (
x,
y,
dx,
dy
) {
var me = new Actor("ball");
me.x = x;
me.y = y;
me.dx = dx;
me.dy = dy;
me.width = 5;
me.height = 5;
// user defined functions will go here
me.draw = function(
) {
fill(0, 0, 255);
ellipse(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
bar: function() {
me.dx = -me.dx;
},
bumper: function() {
me.dy = -me.dy;
}
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiple actors with various properties, fields, methods, and collisions", function () {
            var c = new Compiler();
            var template =
            `actors:
    bar x y {
        width: 2,
        height: 20,
        up: me.move 270 5,
        down: me.move 90 5,

        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }
    }

    bumper y {
        x: 0,
        width: 100,
        height: 1
    }

    score x {
        y: 0,
        width: 1,
        height: 100,
        red: 0,

        draw {
            fill me.red 0 0;
            rect me.x me.y me.width me.height;
        }
        
        collisions [
            ball -> { me.red = 255; }
        ]
    }

    ball x y dx dy {
        width: 5,
        height: 5,

        draw {
            fill 0 0 255;
            ellipse me.x me.y me.width me.height;
        }
        
        collisions [
            bar -> { me.dx = -me.dx; }
            bumper -> { me.dy = -me.dy; }
        ]
    }
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
x,
y
) {
var me = new Actor("bar");
me.x = x;
me.y = y;
me.width = 2;
me.height = 20;
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var bumper = function (
y
) {
var me = new Actor("bumper");
me.y = y;
me.x = 0;
me.width = 100;
me.height = 1;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
x
) {
var me = new Actor("score");
me.x = x;
me.y = 0;
me.width = 1;
me.height = 100;
me.red = 0;
// user defined functions will go here
me.draw = function(
) {
fill(me.red, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
ball: function() {
me.red = 255;
}
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
x,
y,
dx,
dy
) {
var me = new Actor("ball");
me.x = x;
me.y = y;
me.dx = dx;
me.dy = dy;
me.width = 5;
me.height = 5;
// user defined functions will go here
me.draw = function(
) {
fill(0, 0, 255);
ellipse(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
bar: function() {
me.dx = -me.dx;
},
bumper: function() {
me.dy = -me.dy;
}
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles setup function 1 call statement, 0 params", function () {
            var c = new Compiler();
            var template =
            `setup 300 300:
    foo;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
foo();
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles setup function 1 call statement, 1 params", function () {
            var c = new Compiler();
            var template =
            `setup 300 300:
    foo 100;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
foo(100);
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles setup function 1 call statement, multiple params", function () {
            var c = new Compiler();
            var template =
            `setup 300 300:
    size 100 200;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
size(100, 200);
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles setup function 1 call statement, various params", function () {
            var c = new Compiler();
            var template =
            `setup 300 300:
    foo 100 'foo' 6.8;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
foo(100, 'foo', 6.8);
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles setup function multiple call statement, 0 params", function () {
            var c = new Compiler();
            var template =
            `setup 300 300:
    foo;
    bar;
    baz;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
foo();
bar();
baz();
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with no properties and a simple setup function", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {  
    }

setup 300 300:
    foo;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
foo();
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor with no properties and a setup for that actor", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {  
    }

setup 300 300:
    foo: bar;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
var foo = bar();
stage.addActor(foo);
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor and a setup for that actor", function () {
            var c = new Compiler();
            var template =
            `actors: 
    ball x y dx dy {
        width: 5,
        height: 5,

        draw {
            fill 0 0 255;
            ellipse me.x me.y me.width me.height;
        }
        
        collisions [
            bar -> { me.dx = -me.dx; }
            bumper -> { me.dy = -me.dy; }
        ]
    }

setup 300 300:
    baller: ball 0 0 20 10;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var ball = function (
x,
y,
dx,
dy
) {
var me = new Actor("ball");
me.x = x;
me.y = y;
me.dx = dx;
me.dy = dy;
me.width = 5;
me.height = 5;
// user defined functions will go here
me.draw = function(
) {
fill(0, 0, 255);
ellipse(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
bar: function() {
me.dx = -me.dx;
},
bumper: function() {
me.dy = -me.dy;
}
};
return me;
};
// exit actor def
// exit actors
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
var baller = ball(0, 0, 20, 10);
stage.addActor(baller);
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor and a setup for that actor with a keymapping", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        up: me.move 270 5
    }

setup 300 300:
    foo: bar [
        W -> up
    ];
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.up = me.move(270, 5);
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
var foo = bar();
stage.registerKeyPersistentCallback("W", foo.up);
stage.addActor(foo);
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a single actor and a setup for that actor with multiple keymappings", function () {
            var c = new Compiler();
            var template =
            `actors: 
    bar {
        up: me.move 270 5,
        down: me.move 90 5
    }

setup 300 300:
    foo: bar [
        W -> up,
        S -> down
    ];
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
var foo = bar();
stage.registerKeyPersistentCallback("W", foo.up);
stage.registerKeyPersistentCallback("S", foo.down);
stage.addActor(foo);
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles pong.gs", function () {
            var c = new Compiler();
            var template =
            `actors:
    bar x y {
        width: 2,
        height: 20,
        up: me.move 270 5,
        down: me.move 90 5,

        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }
    }

    bumper y {
        x: 0,
        width: 100,
        height: 1
    }

    score x {
        y: 0,
        width: 1,
        height: 100,
        red: 0,

        draw {
            fill me.red 0 0;
            rect me.x me.y me.width me.height;
        }

        collisions [
            ball -> { me.red = 255; }
        ]
    }

    ball x y dx dy {
        width: 5,
        height: 5,

        draw {
            fill 0 0 255;
            ellipse me.x me.y me.width me.height;
        }
        
        collisions [
            bar -> { me.dx = -me.dx; }
            bumper -> { me.dy = -me.dy; }
        ]
    }

setup 300 300:
    p1: bar 0 0 [
        W -> up,
        S -> down
    ];

    p2: bar 100 0 [
        UP -> up,
        DOWN -> down
    ];

    top: bumper 0;
    bottom: bumper 99;

    left: score 0;
    right: score 99;

    baller: ball 0 0 20 10;
`;
            var expected =
            `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
x,
y
) {
var me = new Actor("bar");
me.x = x;
me.y = y;
me.width = 2;
me.height = 20;
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var bumper = function (
y
) {
var me = new Actor("bumper");
me.y = y;
me.x = 0;
me.width = 100;
me.height = 1;
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var score = function (
x
) {
var me = new Actor("score");
me.x = x;
me.y = 0;
me.width = 1;
me.height = 100;
me.red = 0;
// user defined functions will go here
me.draw = function(
) {
fill(me.red, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
ball: function() {
me.red = 255;
}
};
return me;
};
// exit actor def
// enter actor def
var ball = function (
x,
y,
dx,
dy
) {
var me = new Actor("ball");
me.x = x;
me.y = y;
me.dx = dx;
me.dy = dy;
me.width = 5;
me.height = 5;
// user defined functions will go here
me.draw = function(
) {
fill(0, 0, 255);
ellipse(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
bar: function() {
me.dx = -me.dx;
},
bumper: function() {
me.dy = -me.dy;
}
};
return me;
};
// exit actor def
// exit actors
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
var p1 = bar(0, 0);
stage.registerKeyPersistentCallback("W", p1.up);
stage.registerKeyPersistentCallback("S", p1.down);
stage.addActor(p1);
var p2 = bar(100, 0);
stage.registerKeyPersistentCallback("UP", p2.up);
stage.registerKeyPersistentCallback("DOWN", p2.down);
stage.addActor(p2);
var top = bumper(0);
stage.addActor(top);
var bottom = bumper(99);
stage.addActor(bottom);
var left = score(0);
stage.addActor(left);
var right = score(99);
stage.addActor(right);
var baller = ball(0, 0, 20, 10);
stage.addActor(baller);
}
function draw() {
background(200);
stage.tick();
}
`;
            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });
    });

    describe("Expressions", function () {
       it("compiles integers", function() {
           var c = new Compiler();
           var template =
               `actors:
    bar {
        test {
            me.x = 1;
        }
    }
`;
           var expected =
               `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = 1;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

           var render = c.compile(template);
           expect(render).to.be.equal(expected);
           expect(validateJS(render)).to.be.true;
       });

        it("compiles floats", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test {
            me.x = 3.14;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = 3.14;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles strings", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test {
            me.x = 'pi';
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = 'pi';
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles true", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test {
            me.x = true;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = true;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles false", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test {
            me.x = false;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = false;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles parenthesis", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test {
            me.x = (2);
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = (2);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles negation", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test {
            me.x = -2;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = -2;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles not", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test {
            me.x = !true;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = !true;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles multiplication", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a * b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a*b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles division", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a / b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a/b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles addition", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a + b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a+b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles subtraction", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a - b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a-b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles less than", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a < b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a<b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles greater than", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a > b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a>b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles less than or equal", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a <= b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a<=b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles greater than or equal", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a >= b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a>=b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles equal", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a == b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a==b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles not equal", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a != b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a!=b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles and", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a && b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a&&b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles or", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = a || b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = a||b;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles function calls", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.x = me.func a b;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.x = me.func(a, b);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles function calls as arguments", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            me.fun (Math.random());
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
me.fun((Math.random()));
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles object property access", function() {
           var c = new Compiler();
           var template =
`actors:
    foo obj {
        bar {
            fakeFun 1 2 obj.y;
        }
    }
`;
           var expected =
`var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var foo = function (
obj
) {
var me = new Actor("foo");
me.obj = obj;
// user defined functions will go here
me.bar = function(
) {
fakeFun(1, 2, obj.y);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

           var render = c.compile(template);
           expect(render).to.be.equal(expected);
           expect(validateJS(render)).to.be.true;
       });

        it("compiles object property access function calls", function() {
            var c = new Compiler();
            var template =
                `actors:
    foo obj {
        bar {
            obj.fun 1 2;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var foo = function (
obj
) {
var me = new Actor("foo");
me.obj = obj;
// user defined functions will go here
me.bar = function(
) {
obj.fun(1, 2);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });
    });

    describe("Statements", function() {
        it("compiles actor statements with complicated arguments", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar x y {
        width: 2,
        height: 20,
        up: me.move 270 5,
        down: me.move 90 5,

        draw {
            fill 255 0 0;
            rect me.x me.y me.width me.height;
        }
    }
    
setup 300 300:
	test: bar WIDTH 0;
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
x,
y
) {
var me = new Actor("bar");
me.x = x;
me.y = y;
me.width = 2;
me.height = 20;
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
me.draw = function(
) {
fill(255, 0, 0);
rect(me.x, me.y, me.width, me.height);
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
var test = bar(WIDTH, 0);
stage.addActor(test);
}
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles if statements", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            if (a == b) {
                me.x = 0;   
            }
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
if(a==b){
me.x = 0;
}
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles if else statements", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            if (a == b) {
                me.x = 0;   
            } else {
                me.x = 1;
            }
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
if(a==b){
me.x = 0;
}else{
me.x = 1;
}
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles if else if statements", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            if (a == b) {
                me.x = 0;   
            } else if (a > b) {
                me.x = 1;
            }
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
if(a==b){
me.x = 0;
}else if(a>b){
me.x = 1;
}
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles if else if else statements", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            if (a == b) {
                me.x = 0;   
            } else if (a > b) {
                me.x = 1;
            } else {
                me.x = 2;
            }
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
if(a==b){
me.x = 0;
}else if(a>b){
me.x = 1;
}else{
me.x = 2;
}
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles while statements", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        test a b {
            while (a > b) {
                a = a - 1;
            }
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
a,
b
) {
while(a>b){
a = a-1;
}
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });
    });

    describe("Annotations", function() {
        it("compiles a constant function property", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        $test: me.move 0 0
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.test = me.move(0, 0);
me.addConstantFun(me.test);
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        });

        it("compiles a constant function method", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
        $test {
            me.x = me.x + 2;
        }
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
me.test = function(
) {
me.x = me.x+2;
};
me.addConstantFun(me.test);
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        })
    });

    describe("Comments", function() {
        it("compiles comments", function() {
            var c = new Compiler();
            var template =
                `actors:
    bar {
    }
    /*I have some comments here
    and they have multiple lines*/
    foo {
    }
`;
            var expected =
                `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// enter actor def
var foo = function (
) {
var me = new Actor("foo");
// user defined functions will go here
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function draw() {
background(200);
stage.tick();
}
`;

            var render = c.compile(template);
            expect(render).to.be.equal(expected);
            expect(validateJS(render)).to.be.true;
        })
    });

    describe("Key callbacks", function() {
       it("compiles up key callbacks", function() {
           var c = new Compiler();
           var template =
               `actors:
    bar {
        up: me.move 270 5,
        down: me.move 90 5,
        jump {
            me.y = me.y + 10;
        }
    }
    
setup 300 300:
    b: bar [
        W -> up,
        S -> down
    ] [
        X -> jump,
        C -> jump
    ];
`;
           var expected =
               `var WIDTH, HEIGHT;
var stage;
// enter actors
// enter actor def
var bar = function (
) {
var me = new Actor("bar");
me.up = me.move(270, 5);
me.down = me.move(90, 5);
// user defined functions will go here
me.jump = function(
) {
me.y = me.y+10;
};
// the collision map will go here
me.collisionMap = {
};
return me;
};
// exit actor def
// exit actors
function setup() {
WIDTH = 300;
HEIGHT = 300;
createCanvas(WIDTH, HEIGHT);
stage = new Stage(WIDTH, HEIGHT);
var b = bar();
stage.registerKeyPersistentCallback("W", b.up);
stage.registerKeyPersistentCallback("S", b.down);
stage.registerKeyUpCallback("X", b.jump);
stage.registerKeyUpCallback("C", b.jump);
stage.addActor(b);
}
function draw() {
background(200);
stage.tick();
}
`;

           var render = c.compile(template);
           expect(render).to.be.equal(expected);
           expect(validateJS(render)).to.be.true;
       });
    });
});

function validateJS(code) {
    var good = true;
    try {
        var testFunc = function() { new Function(code); };
        testFunc();
    } catch(e) {
        good = false;
    }
    return good;
};