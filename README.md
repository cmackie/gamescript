Pre-proposal:
	https://docs.google.com/a/uw.edu/document/d/1FVXlSsTNWSB6MBmzp_4FKKGq8WDp8cvdB7abjTr_W-M/edit?usp=sharing

Proposal:
	https://docs.google.com/a/uw.edu/document/d/1HdOj4ipnx9-0hASx7I9_7GNpjSw-2Vx6rgr1QAvs-0g/edit?usp=sharing

Design:
	https://docs.google.com/a/uw.edu/document/d/1P9A_lAPnr-zNGsCySDM5jdfQj4TgzIcs1tZO2oFlIYo/edit?usp=sharing

Report:
	https://docs.google.com/a/uw.edu/document/d/1IdNqYKSkybTWLn2vmJiEbc6yhTbZaYwE0aEFbBhRZ_I/edit?usp=sharing

Tutorial:
	https://docs.google.com/a/uw.edu/document/d/1wDX8OmwfHUWsYvXvjZ7eFGUTCUM_JrJCyjHRntrmw1o/edit?usp=sharing

Presentation Slides:
	https://docs.google.com/a/uw.edu/document/d/1JynL_nphs3O0DnNCoVBkA82Suk-c7yKaGuhni3sk09w/edit?usp=sharing

Demo Screencast:
	http://rocne.com/gs/Demo